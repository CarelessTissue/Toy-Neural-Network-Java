double topLeft[] = {0.0,0.0};
double bottomLeft[] = {0.0,1.0};
double topRight[] = {1.0,0.0};
double bottomRight[] = {1.0,1.0};
double middle[] = {0.5,0.5};

double custom[] = {0.0, 0.0};

double black[] = {0.0};
double white[] = {1.0};
double grey[] = {0.5};
int iter = 0;
//double softmax = 0.7;

Matrix m1 = new Matrix(2,2);
Matrix m2 = new Matrix(2,2);
NeuralNetwork nn;

void setup(){  
  nn = new NeuralNetwork(2,200,1);
  size(600,600);
 // frameRate(1);
}

void draw(){
  
  for(int i=0;i<100;i++){
    nn.train(topLeft, black);
    nn.train(bottomLeft, white);
    nn.train(topRight, white);
    nn.train(bottomRight, black);
    iter += i + 1;
    //nn.train(middle, black);
  }

  int res = 5;
  double cols = width/res;
  double rows = height/res;

  for(int i=0;i<cols;i++){
    for(int j=0;j<rows;j++){
      double x1 = i/cols;
      double x2 = j/rows;
      double inputs[] = {x1,x2};
      double[] p = nn.predict(inputs);
      double y = 255*p[0];
      //if(y>230){ y=255; }
      //if(y<80){ y=0; }
      //if(y>=200 && y<=100){ y=127; }
      fill((int)y);      
      rect(i*res,j*res, res, res);
    }
  }  
  println(frameCount, iter);
}

void mousePressed(){
  nn = new NeuralNetwork(2,4,1);
}

void trial(){
  for(int i=0;i<10000;i++){
    nn.train(topLeft, black);
    nn.train(bottomLeft, white);
    nn.train(topRight, white);
    nn.train(bottomRight, black); 
  }
  
  printArray(nn.predict(topLeft)); 
  printArray(nn.predict(bottomLeft));
  printArray(nn.predict(topRight));
  printArray(nn.predict(bottomRight));
}
