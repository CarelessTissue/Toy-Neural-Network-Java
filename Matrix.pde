static class Matrix{
  static double sigmoid(double x){
    //println("x =" + x);
    //println("sigmoid = " + 1 / (1 + Math.exp(-x)));
    return 1 / (1 + Math.exp(-x));
  }

  static double dsigmoid(double y){
    return y * (1 - y);
  }

  int rows;
  int cols;
  double[][] matrix;

// Matrix Constructor
  Matrix(int r, int c){
    rows=r; cols=c;
    matrix = new double[rows][cols];
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        matrix[i][j] = 0.0;
      }
    }
  }

  void map(){
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        double val = matrix[i][j];
        matrix[i][j] = sigmoid(val);
      }
    }
    //show();
  }

  static Matrix dmap(Matrix m){
    Matrix result = m;    
    //m.show();
    for(int i=0; i<result.rows; i++){
      for(int j=0; j<result.cols; j++){
        double val = result.matrix[i][j];
        result.matrix[i][j] = dsigmoid(val);
      }
    }
    /* println("RESULT:");
    result.show(); */
    return result;
  }

  void dmap(){
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        double val = matrix[i][j];
        matrix[i][j] = dsigmoid(val);
      }
    }
    //show();
  }

  static Matrix fromArray(double[] arr){
    Matrix m = new Matrix(arr.length, 1);
    for(int i=0; i<arr.length; i++){
      m.matrix[i][0] = arr[i];
    }    
    return m;
  }

  double[] toArray(){
    double[] arr = new double[rows*cols];
    int c=0;
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        arr[c] = matrix[i][j];
        c++;
      }
    }
    return arr;
  }

// Randomize matrix
  void randomize(){
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        matrix[i][j] = Math.random()*2 - 1;
      }
    }    
  }

// Test matrix
  void test(){
    int c = 0;
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        matrix[i][j] = c*0.1;
        c++;
      }
    }
    show();    
  }

// Transpose matrix
  static Matrix transpose(Matrix m){
    Matrix result = new Matrix(m.cols, m.rows);
    for(int i=0; i<m.rows; i++){
      for(int j=0; j<m.cols; j++){
        result.matrix[j][i] = m.matrix[i][j];
      }
    }
    //result.show();
    return result;
  }

// Scalar Addition of matrices
  Matrix add(double n){    
    Matrix result = new Matrix(rows, cols);
    result.matrix = matrix;
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        result.matrix[i][j] += n;
      }
    }    
    //result.show();
    return result;
  }

/* // Scalar Substraction of matrices
  Matrix substract(double n){    
    Matrix result = new Matrix(rows, cols);
    result.matrix = matrix;
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        result.matrix[i][j] -= n;
      }
    }    
    return result;
  } */

// Scalar Multiplication of Matrices
  Matrix multiply(double n){
    Matrix result = new Matrix(rows, cols);
    result.matrix = matrix;
    for(int i=0; i<rows; i++){
      for(int j=0; j<cols; j++){
        result.matrix[i][j] *= n;
      }
    }
    //result.show();
    return result;
  }

// Element-wise Addition of Matrices
  Matrix add(Matrix n){    
    if(n.rows == rows && n.cols == cols){
      Matrix result = new Matrix(rows, cols);
      result.matrix = matrix;
      for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
          result.matrix[i][j] += n.matrix[i][j];
        }
      }
      //result.show();
      return result;
    }
    else{ 
          println("Matrices dimensions are not same!"); 
          println("Matrices to blame :-");
          show();
          println("=================");
          n.show();
          return null; 
        }  
  }

// Element-wise Substraction of Matrices
  static Matrix substract(Matrix a, Matrix b){    
    if(a.rows == b.rows && a.cols == b.cols){
      Matrix result = new Matrix(a.rows, a.cols);
      
      for(int i=0; i<result.rows; i++){
        for(int j=0; j<result.cols; j++){
          result.matrix[i][j] = a.matrix[i][j] - b.matrix[i][j];
        }
      }
      //result.show();
      return result;
    }
    else{ println("Matrices dimensions are not same!"); return null; }  
  }

// Hadamard Multiplication of Matrices
  Matrix multiply(Matrix n){    
    if(n.rows == rows && n.cols == cols){
      Matrix result = new Matrix(rows, n.cols);
      result.matrix = matrix;
      for(int i=0; i<rows; i++){
        for(int j=0; j<cols; j++){
          result.matrix[i][j] *= n.matrix[i][j];
        }
      }
      //result.show();
      return result;
    }
    else{ println("Matrices dimensions are not same!"); return null; }  
  }

// Dot Multiplication of Matrices
  static Matrix multiply(Matrix a, Matrix b){
    if(a.cols == b.rows){      
      Matrix result = new Matrix(a.rows, b.cols);
      for(int i=0; i<result.rows; i++){
        for(int j=0; j<result.cols; j++){          
          double elem = 0;
          for(int k=0; k<a.cols; k++){
            elem += a.matrix[i][k] * b.matrix[k][j];
          }
          result.matrix[i][j] = elem;
        }
      }
      //result.show();
      return result;
    }

    else{ println("Columns of A must match rows of B!"); 
          println("Matrix in error :-");
          b.show();
          return null; }  
  }

  void show(){    
    for(int i=0; i<rows; i++){      
      for(int j=0; j<cols; j++){
        print(matrix[i][j]+"\t\t\t\t\t\t\t\t\t");
      }
      println();
    }
    println();
  }
}