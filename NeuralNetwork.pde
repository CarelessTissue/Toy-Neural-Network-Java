
class NeuralNetwork{
  
  int input_nodes=0, hidden_nodes=0, output_nodes=0;
  Matrix weights_ih, weights_ho, bias_h, bias_o;
  double lr;
  
  NeuralNetwork(int in_nodes,int hid_nodes,int out_nodes){
    
    input_nodes=in_nodes;
    hidden_nodes=hid_nodes;
    output_nodes=out_nodes;

    weights_ih = new Matrix(hidden_nodes, input_nodes);
    weights_ho = new Matrix(output_nodes, hidden_nodes);
    weights_ih.randomize();
    weights_ho.randomize();   

    bias_h = new Matrix(hidden_nodes, 1);
    bias_o = new Matrix(output_nodes, 1);
    //bias_h.randomize();
    //bias_o.randomize();
    
    lr = 0.1;

  }

  double[] predict(double[] input_array){
    
    double guess;

    Matrix inputs = Matrix.fromArray(input_array);
    
    Matrix hidden = Matrix.multiply(weights_ih, inputs);
    
    hidden.add(bias_h); 
    hidden.map(); 

    Matrix output = Matrix.multiply(weights_ho, hidden);
    

    output.add(bias_o); 
    output.map();  
    
    return output.toArray();
  }

  void train(double[] input_arr, double[] target_arr){
    //Generating the Hidden Outputs
    Matrix inputs = Matrix.fromArray(input_arr);
    Matrix hidden = Matrix.multiply(weights_ih, inputs);
    
    hidden.add(bias_h);
    // activation function!
    
    hidden.map();
    
    // Generating the output's output!
    Matrix outputs = Matrix.multiply(weights_ho, hidden);
    outputs.add(bias_o);
    outputs.map();
    
    // Convert array to matrix object
    Matrix targets = Matrix.fromArray(target_arr);
    
    //Calculate the error
    // ERROR = TARGETS - OUTPUTS
    Matrix output_errors = Matrix.substract(targets, outputs);
    
    //output_errors.show();
    //Calculate gradient

    
    Matrix tempG_outputs = outputs;
    
    tempG_outputs.dmap();
    Matrix gradients = tempG_outputs;
    
    gradients.multiply(output_errors);
    gradients.multiply(lr);
    //gradients.show();

    // Calculate deltas
    Matrix hidden_T = Matrix.transpose(hidden);
    Matrix weights_ho_deltas = Matrix.multiply(gradients, hidden_T);     
    
    weights_ho.add(weights_ho_deltas);
    bias_o.add(gradients);
    //weights_ho.show();
    //bias_o.show();
    //Calculate the hidden layer errors
    Matrix who_t = Matrix.transpose(weights_ho);
    Matrix hidden_errors = Matrix.multiply(who_t,output_errors);

    Matrix tempG_hidden = hidden;
    tempG_hidden.dmap();
    Matrix hidden_gradients = tempG_hidden;
    hidden_gradients.multiply(hidden_errors);
    hidden_gradients.multiply(lr);
    //hidden_gradients.show();
    Matrix input_T = Matrix.transpose(inputs);
    Matrix weights_ih_deltas = Matrix.multiply(hidden_gradients, input_T);

    weights_ih.add(weights_ih_deltas);
    bias_h.add(hidden_gradients);
    
  }

  void train_debug(double[] input_arr, double[] target_arr){
    //Generating the Hidden Outputs
    Matrix inputs = Matrix.fromArray(input_arr);
    Matrix hidden = Matrix.multiply(weights_ih, inputs);
    
    
    hidden.add(bias_h);
    // activation function!
   
    hidden.map();
    
    // Generating the output's output!
    Matrix outputs = Matrix.multiply(weights_ho, hidden);
    outputs.add(bias_o);
    outputs.map();

    Matrix targets = Matrix.fromArray(target_arr);
    Matrix output_errors = Matrix.substract(targets, outputs);
    
    Matrix tempG_outputs = outputs;
    Matrix gradients = Matrix.dmap(tempG_outputs);
    
    gradients.multiply(output_errors);
    gradients.multiply(lr);
    
    // Calculate deltas
    Matrix hidden_T = Matrix.transpose(hidden);
    Matrix weights_ho_deltas = Matrix.multiply(gradients, hidden_T);     

    weights_ho.add(weights_ho_deltas);
    bias_o.add(gradients);

    //Calculate the hidden layer errors
    Matrix who_t = Matrix.transpose(weights_ho);
    Matrix hidden_errors = Matrix.multiply(who_t,output_errors);

    Matrix tempG_hidden = hidden;
    Matrix hidden_gradients = Matrix.dmap(tempG_hidden);
    hidden_gradients.multiply(hidden_errors);
    hidden_gradients.multiply(lr);

    Matrix input_T = Matrix.transpose(inputs);
    Matrix weights_ih_deltas = Matrix.multiply(hidden_gradients, input_T);

    weights_ih.add(weights_ih_deltas);
    bias_h.add(hidden_gradients);

  }

  void visualize(){
    
  }

}
